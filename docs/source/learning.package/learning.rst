learning
========

Subpackages
-----------

.. toctree::

    learning.templatetags

Models
------

.. automodule:: learning.models
    :members:
    :show-inheritance:

Exceptions
----------

.. automodule:: learning.exc
    :members:
    :show-inheritance:
