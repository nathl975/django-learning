.. _theme:

Theme
=====

``django-koalalms-learning`` is a **Django** reusable application. This also means that customizing templates is simple. You can put in your project ``templates`` directory another one, called ``learning`` and write your own templates. Easy.

Overview
########

Here is the list of templates called by views. Ensure you respect the organization that’s given below.

.. code-block::

    .
    ├── base.html
    ├── learning
    │   ├── activity
    │   │   ├── add.html
    │   │   ├── base.html
    │   │   ├── delete.html
    │   │   ├── detail.html
    │   │   ├── details
    │   │   │   ├── add_resource.html
    │   │   │   ├── attach_resource.html
    │   │   │   ├── change.html
    │   │   │   ├── resource.html
    │   │   │   ├── similar.html
    │   │   │   └── usage.html
    │   │   └── my_list.html
    │   ├── base.html
    │   ├── course
    │   │   ├── add.html
    │   │   ├── base.html
    │   │   ├── delete.html
    │   │   ├── detail.html
    │   │   ├── details
    │   │   │   ├── activities.html
    │   │   │   ├── activity_resource.html
    │   │   │   ├── add_activity_on_course.html
    │   │   │   ├── attach_activity.html
    │   │   │   ├── change.html
    │   │   │   ├── collaborators.html
    │   │   │   ├── similar.html
    │   │   │   └── students.html
    │   │   ├── search.html
    │   │   ├── student.html
    │   │   └── teaching.html
    │   ├── dropdown.html
    │   ├── index.html
    │   └── resource
    │       ├── add.html
    │       ├── base.html
    │       ├── delete.html
    │       ├── detail.html
    │       ├── details
    │       │   ├── change.html
    │       │   ├── similar.html
    │       │   └── usage.html
    │       └── my_list.html
    └── markdownx
        └── widget2.html


Root templates
##############

At the ``learning`` root directory, there are three files you can change:

* ``base.html``: the templates that all of your other templates should inherit from.
* ``dropdown.html``: the dropdown you can include in your main project.
* ``index.html``: the ``learning`` index page.


Specific architecture for models
################################

Three models are defined in the application, **course**, **activity** and **resource**. For each model, there are four minimum templates: ``add.html``, ``delete.html``, ``detail.html`` and ``details/change.html``. They are used to perform usual CRUD operations: add, delete, view and delete. In ``details``, there are templates that should inherit from ``detail.html``. In fact, they are about the instance detail: similar objects (``similar.html``), where the resources are used (``usage.html``), etc.

Templates for each model are located in a subdirectory called **course**, **activity** or **resource**. For instance, the views that manage templates under the ``learning/course`` are the :ref:`course_views`, for ``learning/activity``, its :ref:`activity_views` and for ``learning/resource``, it’s :ref:`resource_views`.

Other templates are managed by views in the :ref:`views` module.

