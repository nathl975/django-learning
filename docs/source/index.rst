.. image:: https://www.koala-lms.org/img/banner.png
   :target: https://www.koala-lms.org
   :width: 500 px
   :alt: Koala Learning Management System Logo
   :align: center

django-koalalms-learning
========================

``django-learning`` is a `Django <https://djangoproject.com>`_ application and part of the `Koala Learning Management System (LMS) <https://www.koala-lms.org>`_ project.

It aims to manage **educational resources** in a simple workflow:

* A **Course** is seen as an aggregation of **Activities**. **Students** register on courses and can access to them. The course *owners* can add **Collaborators** to their courses.
* Each **Activity** provides **Educational Resources**. An **Activity** is an aggregation of **Resources**. They can be reused in a **Course**, depending on the owner *restrictions* and *reuse conditions*.
* **Resources** can be videos, texts, documents, links, news articles, etc. **Resources** are reusable objects that can be shared in multiple activities, depending on the owner *restrictions* and *reuse conditions*.

.. important:: This code is about to be production ready, even if still in active development. If you wish to contribute, consider joining us on `Gitlab <https://gitlab.com/koala-lms/django-learning>`_.

As ``django-learning`` is a Django reusable application, you can include it in your own Django project. To install it, see :ref:`installation`. Otherwise, you can use the :ref:`demo-server` that run the application alone on a sample Django project.


Introduction
============

Installation instructions on how to add ``django-learning`` into your own project.

.. toctree::
   :maxdepth: 4

   introduction


Customize
=========

``django-learning`` can be customized easily. As any other **Django** application, you can override templates. The ``django-learning`` templates are distributed under the ``learning`` namespace.

.. toctree::
   :maxdepth: 4

   customize


API Documentation
=================

The ``django-learning`` API documentation will help you dive into the code.

.. warning::
   This documentation is updated against the project **master** branch only. If you wish to get the documentation for the current developments, you’ve got to build it by yourself.

.. toctree::
   :maxdepth: 4

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
