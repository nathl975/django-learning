.. _installation:

Installation instructions
=========================

* ``django-learning`` is available on `Pypi <https://pypi.org/project/django-koalalms-learning/>`_. You can then install it using ``pip``.

.. code-block:: bash

    pip install django-koalalms-learning

* Add ``learning`` and its **dependencies** to your ``INSTALLED_APPS`` setting like this: ::

    INSTALLED_APPS = [
        # django-koala-lms-learning application itself
        'learning',

        # django-koala-lms-learning dependencies
        'taggit',
        'markdownx',
        'django_bootstrap_breadcrumbs',
        'django_countries',
        ...
    ]


.. note::
    Why does ``django-learning`` need all those dependencies?

    * **taggit** is used to let the users add *tags* to their **Courses**, **Activities** and **Resources**.
    * **markdownx** is used to live-convert Markdown text in the **Course**, **Activity** and **Resource** descriptions.
    * **django_boostrap_breadcrumbs** helps us to display a well formed breadcrumb on top of our pages.
    * **django_countries** is used to display flags, depending on the object declared languages.

* Include the ``learning`` and ``markdownx`` URLconf in your project ``urls.py`` like this: ::

    from django.urls import path, include

    path('learning/', include('learning.urls', namespace='learning')),
    path('markdownx/', include('markdownx.urls'))

* ``taggit`` should be case insensitive, so add: ::

    TAGGIT_CASE_INSENSITIVE = True


* Run ``python manage.py migrate`` to create the models.

* Start the development server and visit http://127.0.0.1:8000/admin/ to manage create accounts and courses, activities…

* Visit http://127.0.0.1:8000/learning/ to access the application.

Use in your project
===================

Use the navbar dropdown
-----------------------

If you use **Bootstrap 4** in your **Django project**, with a ``navbar``, you should consider including the template ``learning/dropdown.html`` to your ``base.html`` template. This way, you’ll have all the links to major parts of the ``django-learning`` application.

.. code-block:: html

    <header>
        <nav class="navbar navbar-expand-lg navbar-expand-md navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="{% url 'learning:index' %}">Learning Application</a>

            <ul class="navbar-nav">
                <li class="nav-item">
                    {% include 'learning/dropdown.html' %}
                </li>
            </ul>
        </nav>
    </header>


Point to ``learning`` URLs
--------------------------

Otherwise, you can decide to write your own templates with ``url`` pointing to the application. The URL you should link to are:

.. csv-table::
   :header: "URL", "Name", "Details"
   :widths: 20, 20, 10

   "``course/study`` ", "``learning:course/my``", "List courses followed owned by the current user"
   "``course/teach``", "``learning:course/teaching``", "List courses the current user taught or collaborates on"
   "``course/search``", "``learning:course/search``", "Search for a course"
   "``activity/``", "``learning:activity/my``", "List activities owned by the current user"
   "``resource/``", "``learning:resource/my``", "List resources owned by the current user"
